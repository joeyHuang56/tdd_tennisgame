<?php


namespace App;


class TennisGame
{
    private $player1Name;
    private $player2Name;

    private $player1Score = 0;
    private $player2Score = 0;

    private $scoreMapping = [
        0 => "Love",
        1 => "Fifteen",
        2 => "Thirty",
        3 => "Forty"
    ];

    /**
     * TennisGame constructor.
     */
    public function __construct($player1Name, $player2Name)
    {
        $this->player1Name = $player1Name;
        $this->player2Name = $player2Name;
    }

    public function score()
    {
        if ($this->isTheSameScore())
        {
            return ($this->isDeuce()) ?
                "Deuce" :
                $this->scoreMapping[$this->player1Score] . " all";
        }

        if ($this->isGameBeginning())
        {
            return $this->getFullScoreMapping();
        }

        return ($this->isGamePoint()) ?
            $this->getLeadPlayerName() . " Adv" :
            $this->getLeadPlayerName() . " Win" ;
    }

    public function increasePlayer1Score()
    {
        $this->player1Score++;
    }

    public function increasePlayer2Score()
    {
        $this->player2Score++;
    }

    public function getPlayer1Name()
    {
        return $this->player1Name;
    }

    public function getPlayer2Name()
    {
        return $this->player2Name;
    }

    public function getLeadPlayerName()
    {
        return ($this->player1Score > $this->player2Score) ?
            $this->player1Name :
            $this->player2Name ;
    }

    private function getFullScoreMapping()
    {
        return $this->scoreMapping[$this->player1Score] . " " . strtolower($this->scoreMapping[$this->player2Score]);
    }

    private function isGameBeginning()
    {
        return $this->player1Score <= 3 && $this->player2Score <= 3;
    }

    private function isDeuce()
    {
        return $this->player1Score >= 3;
    }

    private function isTheSameScore()
    {
        return $this->player1Score == $this->player2Score;
    }

    private function isGamePoint()
    {
        return abs($this->player1Score - $this->player2Score) === 1;
    }
}